import org.apache.log4j.BasicConfigurator;
import smile.data.Attribute;
import smile.data.AttributeDataset;
import smile.data.NumericAttribute;
import smile.data.parser.DelimitedTextParser;
import smile.regression.OLS;
import sun.tools.jstat.ParserException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class Exercicio7 {

    public static void main(String[] args) {

        BasicConfigurator.configure();

        DelimitedTextParser parser = new DelimitedTextParser();
        parser.setColumnNames(true);
        parser.setDelimiter(";");
        parser.setCommentStartWith("#");
        parser.setResponseIndex(new NumericAttribute("length"),2);

        Attribute[] attributes = new Attribute[2];
        attributes[0]= new NumericAttribute("age");
        attributes[1]= new NumericAttribute("water_temperature");

        try{
            AttributeDataset dataset = parser.parse(attributes, new File("/Users/fabiofernandes/IdeaProjects/Curso Formação Java Data Science/Analise Preditiva com Machine Learning/projetocapitulo4/src/main/datasets/peixes.csv"));

            double[][] x = dataset.toArray(new double[dataset.size()][]);
            double[] y = dataset.toArray(new double[dataset.size()]);
            OLS regr = new OLS.Trainer().train(x,y);

            System.out.println(String.format("R2: %s", regr.RSquared()));
        }catch (IOException e){
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
