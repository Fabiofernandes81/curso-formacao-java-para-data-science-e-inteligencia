import org.apache.log4j.BasicConfigurator;
import smile.data.AttributeDataset;
import smile.data.NumericAttribute;
import smile.data.parser.DelimitedTextParser;
import smile.regression.OLS;
import smile.validation.MSE;
import smile.validation.RMSE;
import smile.validation.RSS;
import validation.MAE;

import java.io.IOException;
import java.text.ParseException;

public class Exercicio6 {

    public static void main(String[] args) {
        BasicConfigurator.configure();

        DelimitedTextParser parser = new DelimitedTextParser();
        parser.setCommentStartWith("#");
        parser.setColumnNames(true);
        parser.setDelimiter(";");
        parser.setResponseIndex(new NumericAttribute("valor"), 1);

        try {
            AttributeDataset dataset = parser.parse("src/main/datasets/1-AutoInsurSweden.txt");

            double[][] x = dataset.toArray(new double[dataset.size()][]);
            double[] y = dataset.toArray(new double[dataset.size()]);

            System.out.println(x[0][0]);
            System.out.println(y[0]);

            OLS regr = new OLS.Trainer().train(x, y);

            double yPred = regr.predict(new double[]{18});

            System.out.println(String.format("Valor previsto para 18 solicitações: %s", yPred));
            System.out.println(String.format("Intercept: %s", regr.intercept()));
            System.out.println(String.format("Quantos coefiecientes?: %s", regr.coefficients().length));
            System.out.println(String.format("Coefieciente 1: %s", regr.coefficients()[0]));
            System.out.println(String.format("R2: %s", regr.RSquared()));

            double[] residuos = regr.residuals();
            for (double residuo : residuos) {
                System.out.println(String.format("Residuo: %s",residuo));
            }

            double[] y_Pred = new double[x.length];
            for (int i=0; i<x.length;i++){
                y_Pred[i] = regr.predict(x[i]);
            }

            System.out.println(String.format("108 reinvidicações: %s", regr.predict(new double[]{108})));
            System.out.println(String.format("Residuo da primeira obervação: %s", Math.abs(y[0] - y_Pred[0])));

            double mae = new MAE().measure(y, y_Pred);
            System.out.println(String.format("MAE: %s", mae));

            MSE mse = new MSE();
            System.out.println(String.format("MSE: %s", mse.measure(y,y_Pred)));
            System.out.println(String.format("RMSE: %s", new RMSE().measure(y,y_Pred)));
            System.out.println(String.format("RSS: %s", regr.RSS()));
            System.out.println(String.format("RSS: %s", new RSS().measure(y,y_Pred)));
            System.out.println(String.format("R2: %s", regr.RSquared()));


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
