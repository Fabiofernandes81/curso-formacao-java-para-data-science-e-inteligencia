package validation;

import smile.validation.RegressionMeasure;

public class R2 implements RegressionMeasure {
    public double measure(double[] truth, double[] prediction) {
        if (truth == null || truth.length == 0) {
            throw new IllegalArgumentException("Parâmetro truth deverá conter 1 ou mais elementos.");
        }
        if (prediction == null || prediction.length == 0) {
            throw new IllegalArgumentException("Parâmetro prediction deverá conter 1 ou mais elementos.");
        }

        if (prediction.length != truth.length) {
            throw new IllegalArgumentException("Os arrays devem ter o mesmo tamanho.");
        }

        double yTotal = 0;
        for (int i = 0; i < truth.length; i++) {
            yTotal += truth[i];
        }

        double yMean = yTotal / truth.length;

        double sse = 0, sst = 0;
        for (int i = 0; i < truth.length; i++) {
            sse += Math.pow(truth[i] - prediction[i], 2);
            sst += Math.pow(truth[i] - yMean, 2);
        }
        return 1 - sse / sst;
    }
}
