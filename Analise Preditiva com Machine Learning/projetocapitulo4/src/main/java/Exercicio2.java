import org.apache.log4j.BasicConfigurator;
import smile.data.AttributeDataset;
import smile.data.NumericAttribute;
import smile.data.parser.DelimitedTextParser;
import smile.regression.OLS;

import java.io.IOException;
import java.text.ParseException;

public class Exercicio2 {

    public static void main(String[] args) {

        BasicConfigurator.configure();

        DelimitedTextParser parser = new DelimitedTextParser();
        parser.setCommentStartWith("#");
        parser.setColumnNames(true);
        parser.setDelimiter(";");
        parser.setResponseIndex(new NumericAttribute("valor"), 1);

        try {
            AttributeDataset dataset = parser.parse("src/main/datasets/1-AutoInsurSweden.txt");

            double[][] x = dataset.toArray(new double[dataset.size()][]);
            double[] y = dataset.toArray(new double[dataset.size()]);

            System.out.println(x[0][0]);
            System.out.println(y[0]);

            OLS regr = new OLS.Trainer().train(x, y);

            double yPred = regr.predict(new double[]{18});

            System.out.println(String.format("Valor previsto para 18 solicitações: %s", yPred));


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
