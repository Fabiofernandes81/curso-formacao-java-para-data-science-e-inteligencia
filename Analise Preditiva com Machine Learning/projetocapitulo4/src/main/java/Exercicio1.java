import org.apache.log4j.BasicConfigurator;
import smile.regression.OLS;

public class Exercicio1 {

    public static void main(String[] args) {

        BasicConfigurator.configure();
        double[][] x = new double[][]{{2}, {3}, {4}, {5}, {6}, {7}};

        double[] y = new double[]{800, 1100, 1400, 1500, 1570, 1700};

        OLS regr = new OLS(x,y);

        double yPred = regr.predict(new double[]{3});

        System.out.println(String.format("Valor previsto: %s", yPred));

    }
}
