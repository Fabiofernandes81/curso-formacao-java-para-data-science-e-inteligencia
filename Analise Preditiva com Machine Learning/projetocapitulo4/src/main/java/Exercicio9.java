import org.apache.log4j.BasicConfigurator;
import smile.data.Attribute;
import smile.data.AttributeDataset;
import smile.data.NumericAttribute;
import smile.data.parser.DelimitedTextParser;
import smile.regression.OLS;
import smile.validation.MSE;
import smile.validation.RMSE;
import validation.MAE;
import validation.R2;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class Exercicio9 {

    public static void main(String[] args) {

        BasicConfigurator.configure();

        DelimitedTextParser parser = new DelimitedTextParser();
        parser.setColumnNames(true);
        parser.setDelimiter(",");
        parser.setCommentStartWith("#");
        parser.setResponseIndex(new NumericAttribute("sales"), 3);

        Attribute[] attributes = new Attribute[3];
        attributes[0] = new NumericAttribute("TV");
        attributes[1] = new NumericAttribute("Radio");
        attributes[2] = new NumericAttribute("Newspaper");

        try {
            AttributeDataset dataset = parser.parse(attributes, new File("/Users/fabiofernandes/IdeaProjects/Curso Formação Java Data Science/Analise Preditiva com Machine Learning/projetocapitulo4/src/main/datasets/propaganda.csv"));
            double[][] x = dataset.toArray(new double[dataset.size()][]);
            double[] y = dataset.toArray(new double[dataset.size()]);

            float trainSize = 0.8f;
            int trainSamples = (int) (dataset.size() * trainSize);
            int testSamples = dataset.size() - trainSamples;

            double[][] xTrain = new double[trainSamples][];
            double[][] xTest = new double[testSamples][];
            double[] yTrain = new double[trainSamples];
            double[] yTest = new double[testSamples];

            System.arraycopy(x, 0, xTrain, 0, trainSamples);
            System.arraycopy(x, trainSamples, xTest, 0, testSamples);
            System.arraycopy(y, 0, yTrain, 0, trainSamples);
            System.arraycopy(y, trainSamples, yTest, 0, testSamples);

            OLS regr = new OLS.Trainer().train(xTrain, yTrain);

            double[] yPredTrain = new double[yTrain.length];
            for (int i = 0; i<yTrain.length;i++){
                yPredTrain[i] = regr.predict(xTrain[i]);
            }

            double[] yPredTest = new double[yTest.length];
            for (int i = 0; i<yTest.length;i++){
                yPredTest[i] = regr.predict(xTest[i]);
            }

            System.out.println("---");
            System.out.println(String.format("MAE nos dados de treino: %s", new MAE().measure(yTrain, yPredTrain)));
            System.out.println(String.format("MAE nos dados de teste: %s", new MAE().measure(yTest, yPredTest)));
            System.out.println("---");
            System.out.println(String.format("MSE nos dados de treino: %s", new MSE().measure(yTrain, yPredTrain)));
            System.out.println(String.format("MSE nos dados de teste: %s", new MSE().measure(yTest, yPredTest)));
            System.out.println("---");
            System.out.println(String.format("RMSE nos dados de treino: %s", new RMSE().measure(yTrain, yPredTrain)));
            System.out.println(String.format("RMSE nos dados de teste: %s", new RMSE().measure(yTest, yPredTest)));
            System.out.println("---");
            System.out.println(String.format("R2: %s", regr.RSquared()));
            System.out.println(String.format("R2 (DSA) dados de treino: %s", new R2().measure(yTrain,yPredTrain)));
            System.out.println(String.format("R2( DSA) dados de teste: %s", new R2().measure(yTest,yPredTest)));


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
