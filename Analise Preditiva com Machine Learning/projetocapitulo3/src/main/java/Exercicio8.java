import models.Funcionario;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exercicio8 {

    public static void main(String[] args) {

        List<Funcionario> funcionarios = new ArrayList<>();

        Path csvFile = Paths.get("src/main/java/files/funcionarios.csv");

        try(BufferedReader bufferedReader = Files.newBufferedReader(csvFile, StandardCharsets.UTF_8)){
            CSVFormat csv = CSVFormat.RFC4180.withHeader().withDelimiter(';');
            try(CSVParser csvParser = csv.parse(bufferedReader)){
                Iterator<CSVRecord> iterator = csvParser.iterator();
                iterator.forEachRemaining(rec->{
                    String nome = rec.get("nome");
                    String email = rec.get("email");
                    String empresa = rec.get("empresa");
                    float salario =Float.parseFloat(rec.get("salario"));

                    Funcionario funcionario = new Funcionario(nome,email,empresa,salario);
                    funcionarios.add(funcionario);
                });
            }catch (IllegalArgumentException e){
                System.out.println("Falha ao interpretar arquivo. " + e.getMessage());
            }
        }catch (IOException e){
            System.out.println("Falha ao ler arquivo. " + e.getMessage());
        }

        funcionarios.stream().forEach(f-> System.out.println(f.getNome() + " " + f.getSalario()));

    }
}
