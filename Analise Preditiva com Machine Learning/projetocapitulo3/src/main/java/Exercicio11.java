import com.google.gson.Gson;
import models.Fighter;
import utils.UrlUtils;

import java.util.Arrays;
import java.util.List;

public class Exercicio11 {

    public static void main(String[] args) {

        String jsonString = UrlUtils.getResponseFromUrl("http://ufc-data-api.ufc.com/api/v3/iphone/fighters");
        Gson gson = new Gson();
        List<Fighter> fighters = Arrays.asList(gson.fromJson(jsonString, Fighter[].class));

        for (Fighter fighter: fighters) {
            System.out.println(String.format("%s %s possui %s vitórias", fighter.firstName, fighter.lastName, fighter.wins));
            System.out.println(String.format("Foto: %s", fighter.profileImage));
            System.out.println("");
        }
    }
}
