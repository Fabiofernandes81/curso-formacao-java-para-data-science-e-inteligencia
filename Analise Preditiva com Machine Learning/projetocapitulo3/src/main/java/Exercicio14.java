import com.github.lwhite1.tablesaw.api.QueryHelper;
import com.github.lwhite1.tablesaw.api.Table;
import org.knowm.xchart.*;

import java.io.IOException;

public class Exercicio14 {

    public static void main(String[] args) {

        try{
            Table table = Table.createFromCsv("src/main/java/files/tornadoes_1950-2014.csv");
            table.selectWhere(QueryHelper.column("Fatalities").isGreaterThan(10));

            XYChart chart = new XYChartBuilder().width(600).height(500).title("ScatterPlot").xAxisTitle("Feridos").yAxisTitle("Mortos").build();
            chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Scatter);
            chart.addSeries("Série 1", table.column("Injuries").toDoubleArray(), table.column("Fatalities").toDoubleArray());

            new SwingWrapper(chart).displayChart();


        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
