import models.Curso;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;

public class Exercicio4 {

    public static void main(String[] args) {

        BidiMap<Integer, Curso> catalogo = new TreeBidiMap<Integer, Curso>();
        catalogo.put(3, new Curso("Java Deep Learning", 300));
        catalogo.put(2, new Curso("Java Machine Learning", 200));
        catalogo.put(1, new Curso("Java Fundamentos", 100));

        Curso curso2 = catalogo.get(2);
        System.out.println("Curso 2: " + curso2.getNome());

        BidiMap inverso = catalogo.inverseBidiMap();

        System.out.println(inverso.get(curso2));
    }
}
