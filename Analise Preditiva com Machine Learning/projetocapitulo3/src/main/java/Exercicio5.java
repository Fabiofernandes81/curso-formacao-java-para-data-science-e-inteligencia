import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class Exercicio5 {

    public static void main(String[] args) {

        InputStream inputStream = null;
        try {
            inputStream = new URL("https://www.java.com").openStream();
            System.out.println(IOUtils.toString(inputStream));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

    }
}
