import models.Curso;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.map.HashedMap;

import java.util.HashMap;
import java.util.Map;

public class Exercicio2 {

    public static void main(String[] args) {

        HashedMap<Integer, Curso> catalogo2 = new HashedMap();
        catalogo2.put(1, new Curso("Java Fundamentos", 100));
        catalogo2.put(2, new Curso("Java Machine Learning", 200));
        catalogo2.put(3, new Curso("Java Deep Learning", 300));

        MapIterator<Integer, Curso> mapIterator = catalogo2.mapIterator();
        while (mapIterator.hasNext()) {
            Integer chave = mapIterator.next();
            Curso curso = mapIterator.getValue();

            if (chave == 2) {
                mapIterator.remove();
            } else {
                System.out.println(chave + " => " + curso.getNome());
            }
        }

        System.out.println("---");

        System.out.println("Total de elementos: " + catalogo2.size());

    }
}
