import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.Random;

public class Exercicio8_2 {

    public static void main(String[] args) {

        DescriptiveStatistics statistics = new DescriptiveStatistics();

        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            statistics.addValue(random.nextInt(100));
        }

        System.out.println("Máximo: " + statistics.getMax());
        System.out.println("Mínimo: " + statistics.getMin());
        System.out.println("Soma: " + statistics.getSum());
        System.out.println("Média: " + statistics.getMean());
        System.out.println("Mediana: " + statistics.getPercentile(50));
        System.out.println("Variância: " + statistics.getVariance());
        System.out.println("Desvio Padrão: " + statistics.getStandardDeviation());

    }
}
