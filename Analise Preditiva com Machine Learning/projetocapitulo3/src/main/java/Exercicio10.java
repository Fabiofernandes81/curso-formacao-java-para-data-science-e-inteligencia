import com.google.gson.Gson;
import models.Post;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Exercicio10 {

    public static void main(String[] args) {

        URL url = null;

        String jsonString = "";

        try {
            url = new URL("https://jsonplaceholder.typicode.com/posts/2");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
            String input = null;

            while ((input = bufferedReader.readLine()) != null) {
                jsonString += input;
            }

            bufferedReader.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(jsonString);

        Gson gson = new Gson();
        Post post = gson.fromJson(jsonString, Post.class);
        System.out.println("");
        System.out.println("Título: " + post.getTitle());
        System.out.println("");
        System.out.println("Corpo: " + post.getBody());

    }
}
