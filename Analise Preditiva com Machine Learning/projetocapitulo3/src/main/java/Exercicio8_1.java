import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class Exercicio8_1 {

    public static void main(String[] args) {

        double[][] matrixData = {{2, 3}, {0, 1}, {-1, 4}};
        RealMatrix realMatrix = MatrixUtils.createRealMatrix(matrixData);

        double[][] matrixData2 = {{1, 2, 3}, {-2, 0, 4}};
        RealMatrix realMatrix2 = MatrixUtils.createRealMatrix(matrixData2);

        RealMatrix realMatrix3 = realMatrix.multiply(realMatrix2);

        System.out.println(realMatrix3);

    }
}
