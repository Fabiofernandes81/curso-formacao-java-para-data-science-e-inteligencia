import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Exercicio9_1 {

    public static void main(String[] args) {

        try {
            Document document = Jsoup.connect("https://www.w3schools.com/html/html_lists.asp").get();
            System.out.println(document.text());
            Elements tableRows = document.select(".w3-table-all notranslate tbody tr");

            for (Element element : tableRows) {
                Elements columns = element.select("td");

                if (columns.isEmpty()) {
                    continue;
                }

                String empresa = columns.get(0).text();
                String contato = columns.get(1).text();


                System.out.println(String.format("value %s  description %s", contato, empresa));

            }
        } catch (IOException e) {
            System.out.println("Erro " + e.getMessage());
            e.printStackTrace();
        }
    }
}
