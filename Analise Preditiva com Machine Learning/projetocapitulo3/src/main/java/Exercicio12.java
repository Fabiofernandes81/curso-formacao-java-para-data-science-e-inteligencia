import com.github.lwhite1.tablesaw.api.CategoryColumn;
import com.github.lwhite1.tablesaw.api.Table;
import static com.github.lwhite1.tablesaw.api.QueryHelper.column;

import java.io.IOException;

public class Exercicio12 {

    public static void main(String[] args) {

        try{
            long ti = System.currentTimeMillis();
            Table tornados =  Table.createFromCsv("src/main/java/files/tornadoes_1950-2014.csv");
            long tf = System.currentTimeMillis();

            System.out.println("Carregado em " + (tf - ti) + " ms.");
            System.out.println(String.format("Colunas: %s", tornados.columnNames()));
            System.out.println(String.format("Shape: %s", tornados.shape()));
            System.out.println(String.format("%s X %s", tornados.rowCount(), tornados.columnCount()));
            System.out.println(tornados.structure().print());

            Table tornadosComMais100Mortes = tornados.selectWhere(column("Fatalities").isGreaterThan(100));
            System.out.println(tornadosComMais100Mortes.shape());
            System.out.println(tornadosComMais100Mortes.print());
            System.out.println(tornadosComMais100Mortes.first(2).print());

            CategoryColumn month = tornados.dateColumn("Date").month();
            tornadosComMais100Mortes.addColumn(month);

            tornadosComMais100Mortes.removeColumns("Date");

            tornadosComMais100Mortes = tornadosComMais100Mortes.sortDescendingOn("Injuries");

            System.out.println(tornadosComMais100Mortes.column("Injuries").summary().print());

            Table tornadosNoQuartoTrimestre =
                    tornados.selectWhere(column("Injuries").isGreaterThanOrEqualTo(5))
                            .selectWhere(column("Scale").isGreaterThan(2))
                            .select("Date", "Injuries","Scale")
                            .where(column("Date").isInQ4());

            System.out.println(String.format("Tornados no quarto trimestre com mais de 4 feridos: %s",tornadosNoQuartoTrimestre.print()));
            tornadosNoQuartoTrimestre.exportToCsv("src/main/java/files/tornados_4_trimestre.csv");

            String datasetName = tornados.save("src/main/java/files");

            ti = System.currentTimeMillis();
            Table novoDataset = Table.readTable(datasetName);
            tf = System.currentTimeMillis();
            System.out.println("Carregado em " + (tf - ti) + " ms.");



        }catch (IOException e){
            System.out.println("Ocorreu um erro " + e.getMessage());
            e.printStackTrace();
        }
    }
}
