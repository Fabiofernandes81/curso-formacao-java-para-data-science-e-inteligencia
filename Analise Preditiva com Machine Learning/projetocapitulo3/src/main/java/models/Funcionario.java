package models;

public class Funcionario {

    private String nome;

    private String email;

    private String empresa;

    private float salario;

    public Funcionario(String nome, String email, String empresa, float salario) {
        this.nome = nome;
        this.email = email;
        this.empresa = empresa;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getEmpresa() {
        return empresa;
    }

    public float getSalario() {
        return salario;
    }
}
