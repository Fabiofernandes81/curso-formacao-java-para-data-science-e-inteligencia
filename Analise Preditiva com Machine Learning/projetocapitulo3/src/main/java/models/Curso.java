package models;

public class Curso  implements Comparable<Curso>{

    private String nome;

    private float valor;

    public Curso(String nome, float valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public float getValor() {
        return valor;
    }

    public int compareTo(Curso o) {
        return this.nome.compareTo(o.getNome());
    }
}
