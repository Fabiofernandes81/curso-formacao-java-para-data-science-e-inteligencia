import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;

public class Exercicio1 {

    public static void main(String[] args) {

        Bag bag = new HashBag();
        bag.add("Machine Learning", 8);
        bag.add("Big Data", 2);
        bag.remove("Machine Learning", 3);

        int totalOcorrencias = bag.getCount("Machine Learning");
        System.out.println("Total de Ocorrências " + totalOcorrencias);
    }

}
