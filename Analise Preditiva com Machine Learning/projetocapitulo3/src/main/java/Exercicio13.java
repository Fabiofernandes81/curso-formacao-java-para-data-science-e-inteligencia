import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import java.io.IOException;

public class Exercicio13 {

    public static void main(String[] args) {

        double[] xData = new double[]{0.0, 1.0, 2.0};
        double[] yData = new double[]{2.0, 1.0, 0.0};

        XYChart chart = QuickChart.getChart("Sample Chart", "x", "y", "y(x)", xData, yData);

        new SwingWrapper(chart).displayChart();

        try{
            BitmapEncoder.saveBitmapWithDPI(chart, "src/main/java/files/grafico1.png", BitmapEncoder.BitmapFormat.PNG,300);
        }catch (IOException e){
            e.getMessage();

        }
    }
}
