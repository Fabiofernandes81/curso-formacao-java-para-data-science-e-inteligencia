import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Exercicio9 {

    public static void main(String[] args) {

        try {
            Document document = Jsoup.connect(" https://www.w3schools.com/html/html_tables.asp").get();

            System.out.println("Título da Página : " + document.title());

            Elements tableRows = document.select("#customers tbody tr");

            for (Element element : tableRows) {
                Elements columns = element.select("td");

                if (columns.isEmpty()) {
                    continue;
                }

                String empresa = columns.get(0).text();
                String contato = columns.get(1).text();
                String pais = columns.get(2).text();

                System.out.println(String.format("%s é o contato da empresa %s em %s", contato, empresa, pais));
            }
        } catch (IOException e) {
            System.out.println("Erro " + e.getMessage());
            e.printStackTrace();
        }
    }
}
