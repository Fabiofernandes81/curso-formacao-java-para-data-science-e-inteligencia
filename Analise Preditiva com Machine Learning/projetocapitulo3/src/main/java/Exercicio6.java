import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

public class Exercicio6 {

    public static void main(String[] args) {

        Table<String, String, Integer> catalogo = TreeBasedTable.create();
        catalogo.put("Java Fundamentos", "Créditos", 15);
        catalogo.put("Java Fundamentos", "Valor", 1000);
        catalogo.put("Java Machine Learning", "Créditos", 25);
        catalogo.put("Java Machine Learning", "Valor", 2000);
        catalogo.put("Java Deep Learning", "Créditos", 35);
        catalogo.put("Java Deep Learning", "Valor", 3000);

        System.out.println(catalogo.containsColumn("Valor"));
        System.out.println(catalogo.containsRow("Java Machine Learning"));
        System.out.println(catalogo.contains("Java Machine Learning", "Créditos"));
        System.out.println(catalogo.get("Java Deep Learning", "Valor"));
        catalogo.remove("Java Deep Learning", "Créditos");
        System.out.println(catalogo.contains("Java Deep Learning", "Créditos"));
        System.out.println(catalogo.get("Java Deep Learning", "Créditos"));

    }
}
