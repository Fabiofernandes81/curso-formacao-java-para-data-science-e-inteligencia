import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import com.google.common.primitives.Ints;

public class Exercicio7 {

    public static void main(String[] args) {

        Range<Integer> range1 = Range.closed(0,9);
        System.out.println("[0,9] : ");
        printRange(range1);
        System.out.println("5 está presente? " + range1.contains(5));
        System.out.println("(5,6,7) estão presentes? " + range1.containsAll(Ints.asList(5,6,7)));
        System.out.println("Limite inferior: " + range1.lowerEndpoint());
        System.out.println("Limite superior: " + range1.upperEndpoint());

        Range<Integer> range2 = Range.open(0,9);
        System.out.println("[0,9] : ");
        printRange(range2);

        Range<Integer> range3 = Range.openClosed(0,9);
        System.out.println("[0,9] : ");
        printRange(range3);

        Range<Integer> range4 = Range.closedOpen(0,9);
        System.out.println("[0,9] : ");
        printRange(range4);

        Range<Integer> range5 = Range.greaterThan(9);
        System.out.println("[9, infinito] : ");
        System.out.println("Limite inferior: " + range5.lowerEndpoint());
        System.out.println("Limite superior está presente? " + range5.hasUpperBound());

        Range<Integer> range6 = Range.closed(3,5);
        printRange(range6);
        System.out.println("Intervaro [0,9] possui [3,5]? " + range1.encloses(range6));

        Range<Integer> range7 = Range.closed(5,15);
        printRange(range1.intersection(range7));
        printRange(range1.span(range7));

    }

    private static void printRange(Range<Integer> range){
        System.out.print("[");
        for (int grade : ContiguousSet.create(range, DiscreteDomain.integers())){
            System.out.print(grade + " ");
        }
        System.out.println("]");
    }
}
