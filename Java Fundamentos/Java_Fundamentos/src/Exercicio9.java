import java.util.Scanner;

public class Exercicio9 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Qual a sua idade?");
        int idade = scanner.nextInt();

        if (idade < 0) {
            System.out.println("Idade inválida");
            System.out.println("Tente Novamente");
        } else if (idade < 16) {
            System.out.println("Não pode votar");
        } else if (idade < 18 || idade >= 65) {
            System.out.println("Voto opcional");
        } else {
            System.out.println("Voto obrigatório");
        }
    }
}
