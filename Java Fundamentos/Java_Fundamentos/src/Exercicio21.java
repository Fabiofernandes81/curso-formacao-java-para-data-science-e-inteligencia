public class Exercicio21 {

    public static void main(String[] args){

        String anotherPalindrome = "Niagara. O roar again!";

        char aChar = anotherPalindrome.charAt(9);

        System.out.println(aChar);

        String roar = anotherPalindrome.substring(11, 15);
        System.out.println(roar);

        String nomes = "Joao, José, Maria, Roberto, Silvia";
        String[] arrayNomes = nomes.split(",");
        for (String nome: arrayNomes) {
            System.out.println(nome.trim());
        }

        System.out.println(String.format("%s tem %d letras", anotherPalindrome, anotherPalindrome.length()));

        System.out.println("dsa é incrível".toUpperCase());

        int primeiraOcorrencia = anotherPalindrome.indexOf("ga");
        System.out.println(primeiraOcorrencia);

        int ultimaOcorrencia = anotherPalindrome.indexOf("ga");
        System.out.println(ultimaOcorrencia);

        System.out.println("DSA é incrível ".contains("é"));

        System.out.println("DSA é incrível ".contains("curso ruim"));

        String frase = "seja-bem-vindo-ao-curso-de-java-fundamentos".replace('-', ' ').replaceAll(" ", "  ").replaceFirst("ja","JA");

        System.out.println(frase);



    }

}
