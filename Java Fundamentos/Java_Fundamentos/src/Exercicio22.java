import java.text.SimpleDateFormat;
import java.util.Date;

public class Exercicio22 {

    public static void main(String[] args){

        Date dataAtual = new Date();
        System.out.println(dataAtual.getTime());
        System.out.println(dataAtual);

        Date outraData = new Date(0);
        System.out.println(outraData);

        outraData.setTime(2352525235L);
        System.out.println(outraData);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        System.out.println(sdf.format(dataAtual));

        SimpleDateFormat sdf2 = new SimpleDateFormat("D");
        System.out.println(sdf2.format(dataAtual));

        System.out.println(dataAtual.before(outraData));
        System.out.println(dataAtual.after(outraData));


    }
}
