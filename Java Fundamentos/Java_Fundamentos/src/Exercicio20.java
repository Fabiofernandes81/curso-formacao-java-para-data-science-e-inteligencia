import java.util.Date;

public class Exercicio20 {

    public static void main(String[] args){

        String msg;

        String curso = "Java Fundamentos";

        double valorCurso = 123.456789;

        msg = String.format("Bem vindo ao curso %s da DSA", curso);

        msg = String.format("Bem vindo ao curso %s da DSA. Teremos %d videos", curso, 200);

        msg = String.format("Valor do Curso %s: %f", curso, valorCurso);

        msg = String.format("Valor do Curso %s: %.2f", curso, valorCurso);

        msg = String.format("Hoje é %tD", new Date());

        System.out.println(msg);





    }
}
