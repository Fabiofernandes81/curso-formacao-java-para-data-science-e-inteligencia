import java.util.Scanner;

public class Exercicio11 {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        System.out.println("Tabuada de qual número?");
        int numero = reader.nextInt();

        System.out.println("Até qual número?");
        int ate = reader.nextInt();

        int x = 0;
        while (x <= ate){
            System.out.println(String.format("%s * %s = %s", numero, x, numero * x));
            x++;
        }
    }
}
