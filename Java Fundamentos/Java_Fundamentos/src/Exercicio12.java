import java.util.Scanner;

public class Exercicio12 {

    public static void main(String[] args){

        int idade;

        do{
            Scanner reader = new Scanner(System.in);
            System.out.println("Qual a sua idade?");
            idade = reader.nextInt();
        }

        while (idade < 0);

        System.out.println(String.format("Faltam %s anos para você chegar aos 100 anos!",100 - idade));
    }
}
