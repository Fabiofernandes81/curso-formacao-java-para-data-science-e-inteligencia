import java.util.Scanner;

public class Exercicio14 {

    public static void main(String[] args){

        Scanner reader = new Scanner(System.in);
        System.out.println("Tabuada de qual número?");
        int numero = reader.nextInt();

        for (int x = 0; x<= 10; x++){

            if(x % 2 ==1){
                continue;
            }

            System.out.println(String.format("%s * %s = %s", numero, x, numero * x));

            if(x > 7){
                break;
            }
        }

        System.out.println("Fim da Tabuada!");

    }
}
