import java.util.Calendar;
import java.util.Date;

public class Exercicio23 {

    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar);

        calendar.setTime(new Date());

        int dia = calendar.get(Calendar.DATE);
        int mes = calendar.get(Calendar.MONTH) + 1;
        int ano = calendar.get(Calendar.YEAR);

        System.out.println(String.format("%s/%s/%s", dia, mes, ano));

        calendar.set(Calendar.YEAR, 2018);
        System.out.println(calendar.toInstant().toString());

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DATE, -7);
        System.out.println(calendar.toInstant().toString());

        Calendar outroCalendar = Calendar.getInstance();
        System.out.println(outroCalendar.before(calendar));


    }
}
