public class Exercicio10 {

    public static void main(String[] args){

        int idade = 70;

        float preco = 200;

        if(idade >= 60){
            float precoComDesconto = preco / 100 * 90;
            System.out.println(String.format("Desconto de: %s", preco - precoComDesconto));
            preco = precoComDesconto;
        }
        exibirPrecoFinal(preco);

    }

    private static void exibirPrecoFinal(float preco){
        System.out.println(String.format("Preço Final: %s", preco));
    }
}
