import java.util.Scanner;

public class Exercicio7 {

    public  static void main(String[] args){

        Scanner reader = new Scanner(System.in);
        System.out.println("Qual sua idade?");
        int idade = reader.nextInt();

        if(idade < 0){
            System.out.println("Idade inválida");
        }else{
            System.out.println(String.format("Ok, faltam %s para você chegar aos 100 anos.",(100 -idade)));
        }

    }
}
