public class Exercicio17 {

    public static void main(String[] args){

        String mes = "maio";

        switch (mes){
            case "janeiro":
                System.out.println("Janeiro");
            case "fevereiro":
                System.out.println("Fevereiro");
            case "março":
                System.out.println("Março");
            case "abril":
                System.out.println("abril");
            case "maio":
                System.out.println("Maio");
            case "junho":
                System.out.println("Junho");
            case "julho":
                System.out.println("Julho");
            case "agosto":
                System.out.println("Agosto");
            case "setembro":
                System.out.println("Setembro");
            case "outubro":
                System.out.println("Outubro");
            case "novembro":
                System.out.println("Novembro");
            case "Dezembro":
                System.out.println("Dezembro");
                default:
                    System.out.println("Mês inválido");
        }

    }
}
