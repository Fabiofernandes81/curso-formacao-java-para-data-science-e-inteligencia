public class Exercicio6 {

    public static void main(String[] args){

        int x = 10, y =5;
        System.out.println(String.format("x > y : %s", (x > y)));
        System.out.println(String.format("x < y : %s", (x < y)));
        System.out.println(String.format("x >= y : %s", (x >= y)));
        System.out.println(String.format("x <= y : %s", (x <= y)));
        System.out.println(String.format("x == y : %s", (x == y)));
        System.out.println(String.format("x != y : %s", (x != y)));

    }
}
