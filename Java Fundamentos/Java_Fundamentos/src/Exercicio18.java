public class Exercicio18 {

    public static void main(String[] args){

        int nota1 = 7, nota2 = 7;

        String situacao =  ((nota1 + nota2) / 2 > 6) ? "Aprovado" : "Reprovado";
        System.out.println(situacao);
    }
}
