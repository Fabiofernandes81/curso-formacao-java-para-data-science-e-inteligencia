import java.util.Scanner;

public class Exercicio13 {

    public static void main(String[] args){

        Scanner reader = new Scanner(System.in);
        System.out.println("Tabuada de qual número?");
        int numero = reader.nextInt();

        System.out.println("Até qual número?");
        int ate = reader.nextInt();

        for (int x = 0; x<= ate; x++){
            System.out.println(String.format("%s * %s = %s", numero, x, numero * x));
        }

    }
}
