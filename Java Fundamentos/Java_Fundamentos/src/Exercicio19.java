public class Exercicio19 {

    public static void main(String[] args){

        calcular(10,8, TipoOperacao.ADICAO);

    }

    private static void calcular(int operando1, int operando2, TipoOperacao tipoOperacao){
        if(tipoOperacao == TipoOperacao.ADICAO){
            System.out.println(operando1 + operando2);
        }else if(tipoOperacao == TipoOperacao.SUBTRACAO){
            System.out.println(operando1 - operando2);
        }else if(tipoOperacao == TipoOperacao.MULTIPLICACAO){
            System.out.println(operando1 * operando2);
        }else if(tipoOperacao == TipoOperacao.DIVISAO){
            System.out.println(operando1 / operando2);
        }else{
            System.out.println("Tipo de operaçao inválida");
        }

    }

}

enum TipoOperacao {
    ADICAO, SUBTRACAO, MULTIPLICACAO, DIVISAO
}