import models.Livro;

import java.util.ArrayList;
import java.util.List;

public class Exercicio11 {

    public static void main(String[] args) {

        List<Livro> livroList = new ArrayList<>();
        livroList.add(new Livro(0, "Livro0", 756, 200));
        livroList.add(new Livro(1, "Livro1", 80, 120));
        livroList.add(new Livro(2, "Livro2", 120, 45));
        livroList.add(new Livro(3, "Livro3", 15, 140));
        livroList.add(new Livro(4, "Livro4", 140, 90));
        livroList.add(new Livro(5, "Livro de Teste", 1000, 300));

        livroList.stream()
                .map(Livro::getTitulo)
                .filter(s -> s.startsWith("Livro"))
                .map(String::length)
                .filter(i -> i > 7)
                .forEach(System.out::println);

    }
}
