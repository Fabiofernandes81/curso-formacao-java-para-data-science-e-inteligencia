import models.Livro;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Exercicio10 {

    public static void main(String[] args) {

        List<Livro> livroList = new ArrayList<>();
        livroList.add(new Livro(1, "Livro1", 80, 120));
        livroList.add(new Livro(2, "Livro2", 120, 45));
        livroList.add(new Livro(3, "Livro3", 15, 140));
        livroList.add(new Livro(4, "Livro4", 140, 101));

        Predicate<Livro> maisDe100Paginas = l -> l.getPaginas() > 100;
        Predicate<Livro> precoMaiorQue100Reais = l -> l.getPreco() > 100;

        Predicate<Livro> precoMaiorQue100ReaisEprecoMaiorQue100Reais = maisDe100Paginas.and(precoMaiorQue100Reais);
        Predicate<Livro> precoMaiorQue100ReaisOuprecoMaiorQue100Reais = maisDe100Paginas.or(precoMaiorQue100Reais);

        Predicate<Livro> menosDe100Paginas = maisDe100Paginas.negate();

        System.out.println("Livros com mais de 100 páginas e preço maior que 100 reais.");
        livroList.stream()
                .filter(precoMaiorQue100ReaisEprecoMaiorQue100Reais)
                .forEach(System.out::println);

        System.out.println("---");


        System.out.println("Livros com mais de 100 páginas ou preço maior que 100 reais.");
        livroList.stream()
                .filter(precoMaiorQue100ReaisOuprecoMaiorQue100Reais)
                .forEach(System.out::println);

        System.out.println("---");

        livroList.stream()
                .filter(menosDe100Paginas)
                .forEach(System.out::println);

    }
}
