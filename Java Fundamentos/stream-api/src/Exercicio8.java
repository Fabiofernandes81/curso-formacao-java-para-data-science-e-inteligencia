import java.util.stream.DoubleStream;

public class Exercicio8 {

    public static void main(String[] args) {

        DoubleStream doubleStream = DoubleStream.of(5.3, 2.1, 4.7, 8.8, 9.1, 41, -2.3, 8.3);
        doubleStream.filter(d -> d >= 0 && d <= 10)
                .sorted()
                .limit(4)
                .skip(1)
                .forEach(System.out::println);
    }
}
