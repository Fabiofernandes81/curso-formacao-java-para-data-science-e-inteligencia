package models;

import java.util.ArrayList;
import java.util.List;

public class Time {

    private String nome;

    public List<Jogador> getJogadores() {
        return jogadores;
    }

    private List<Jogador> jogadores;

    public Time(String nome) {
        this.nome = nome;
        this.jogadores = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void adicionarJogador(Jogador jogador){
        this.jogadores.add(jogador);
    }


}
