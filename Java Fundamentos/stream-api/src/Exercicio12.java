import models.Livro;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exercicio12 {

    public static void main(String[] args) {

        List<Livro> livroList = new ArrayList<>();
        livroList.add(new Livro(0, "Livro0", 756, 200));
        livroList.add(new Livro(1, "Livro1", 80, 120));
        livroList.add(new Livro(2, "Livro2", 120, 45));
        livroList.add(new Livro(3, "Livro3", 15, 140));
        livroList.add(new Livro(4, "Livro4", 140, 90));
        livroList.add(new Livro(5, "Livro de Teste", 1000, 300));

        List<Livro> livrosPrecoMaiorQue100 = livroList.stream()
                .filter(l -> l.getPreco() > 100)
                .collect(Collectors.toList());

        System.out.println(livrosPrecoMaiorQue100);

        System.out.println("---");

        String titulosLivros = livrosPrecoMaiorQue100.stream()
                .map(l->l.getTitulo())
                .collect(Collectors.joining(", "));
        System.out.println(titulosLivros);

        System.out.println("---");

        int totalPaginas = livroList.stream().mapToInt(Livro::getPaginas).sum();

        System.out.println("Total de páginas de todos os livros: " + totalPaginas);

    }
}
