import models.Livro;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Exercicio9 {

    public static void main(String[] args) {

        List<Livro> livroList = new ArrayList<>();
        livroList.add(new Livro(1, "Livro1", 80, 120));
        livroList.add(new Livro(2, "Livro2", 120, 45));
        livroList.add(new Livro(3, "Livro3", 15, 140));
        livroList.add(new Livro(4, "Livro4", 140, 90));

        Optional<Livro> optionalLivro = livroList.stream()
                .reduce(
                        (livro1, livro2) ->
                                livro1.getPreco()/livro1.getPaginas() > livro2.getPreco()/livro2.getPaginas() ? livro1 : livro2
                );

        if(optionalLivro.isPresent()){
            System.out.println(optionalLivro.get().getTitulo() + " : " + optionalLivro.get().getPreco());
        }

    }
}
