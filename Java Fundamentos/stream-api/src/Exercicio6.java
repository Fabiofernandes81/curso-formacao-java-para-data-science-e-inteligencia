import java.util.stream.IntStream;

public class Exercicio6 {

    public static void main(String[] args) {

        IntStream intStream = IntStream.of(1, 2, 2, 4, 4, 4, 7, 8, 9, 10);

        intStream.distinct().forEach(System.out::println);
    }
}
