import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Exercicio5 {

    public static void main(String[] args) {

        IntStream intStream = IntStream.of(1, 2, 2, 4, 4, 4, 7, 8, 9, 10);
//        System.out.println(String.format("Total de elementos %d", intStream.count()));

//        System.out.println("Média: " + intStream.average().getAsDouble());

//        System.out.println("Maximo: " + intStream.max().getAsInt());

        OptionalInt optionalInt = intStream.min();

        if (!optionalInt.isPresent()) {
            System.out.println("Não há número mínimo.");
        } else {
            System.out.println("Mínimo: " + optionalInt.getAsInt());
        }

    }
}
