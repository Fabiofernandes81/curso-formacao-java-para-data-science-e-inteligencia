import models.Funcionario;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exercicio14 {

    public static void main(String[] args) {
        List<Funcionario> funcionarios = new ArrayList<>();
        funcionarios.add(new Funcionario("José", 1000D, "Contabilidade"));
        funcionarios.add(new Funcionario("Roberta", 2000D, "Marketing"));
        funcionarios.add(new Funcionario("Márcia", 3000D, "Contabilidade"));
        funcionarios.add(new Funcionario("Vera", 4000D, "Marketing"));
        funcionarios.add(new Funcionario("João", 5000D, "Recursos Humanos"));

        Map<String, Double> mapSalariosPorDepartamento = funcionarios.stream()
                .collect(Collectors.groupingBy(Funcionario::getDepartamento,
                        Collectors.summingDouble(Funcionario::getSalario)));

        for (String departamento : mapSalariosPorDepartamento.keySet()) {
            System.out.println(String.format("Total de salários em %s: %.2f", departamento, mapSalariosPorDepartamento.get(departamento)));
        }

        System.out.println("---");

        Map<String, Long> mapFuncionariosPorDepartamento = funcionarios.stream()
                .collect(Collectors.groupingBy(Funcionario::getDepartamento,
                        Collectors.counting()));

        for (String departamento : mapFuncionariosPorDepartamento.keySet()) {
            System.out.println(String.format("Total de funcionários em %s: %d", departamento, mapFuncionariosPorDepartamento.get(departamento)));
        }

        Map<String, Double> mapMediaSalarialPorDepartamento = funcionarios.stream()
                .collect(Collectors.groupingBy(Funcionario::getDepartamento,
                        Collectors.averagingDouble(Funcionario::getSalario)));
        for (String departamento : mapMediaSalarialPorDepartamento.keySet()) {
            System.out.println(String.format("Média de salário de %s: %.2f", departamento, mapMediaSalarialPorDepartamento.get(departamento)));
        }
    }
}
