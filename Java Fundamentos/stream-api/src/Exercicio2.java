import java.util.function.BiConsumer;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.function.ToIntBiFunction;

public class Exercicio2 {

    public static void main(String[] args) {

        BiConsumer<Integer, Integer> somaInteiros = (a, b) -> System.out.println(a + b);
        somaInteiros.accept(2, 5);

        DoubleFunction<String> doubleFunction = d -> String.valueOf(d * d);
        System.out.println(doubleFunction.apply(3f));

        ToIntBiFunction<Integer, Integer> somaInteiros2 = (a, b) -> a + b;
        System.out.println(somaInteiros2.applyAsInt(3, 10));

        ToIntBiFunction<String, String> totalCaracteres = (a, b) -> a.length() + b.length();
        System.out.println(totalCaracteres.applyAsInt("DSA", "Java"));

        Function<String, Integer> totalCaracteres2 = (i) -> i.length();
        System.out.println(totalCaracteres2.apply("DSA"));


    }
}
