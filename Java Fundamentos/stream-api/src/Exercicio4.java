import java.util.*;

public class Exercicio4 {

    public static void main(String[] args) {

        List<String> nomes = new ArrayList<>(Arrays.asList("Max", "Roberto", "Fabio", "Miriam", "Gislaine"));

        Collections.sort(nomes, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });

        System.out.println(nomes);
        System.out.println("---");
        Collections.shuffle(nomes);

        Collections.sort(nomes, (o1, o2) -> o1.length() - o2.length());

        System.out.println(nomes);

    }
}
