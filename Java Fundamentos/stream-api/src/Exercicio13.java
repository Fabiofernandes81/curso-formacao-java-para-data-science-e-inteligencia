import models.Funcionario;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exercicio13 {

    public static void main(String[] args) {

        List<Funcionario> funcionarios = new ArrayList<>();
        funcionarios.add(new Funcionario("José", 1000D, "Contabilidade"));
        funcionarios.add(new Funcionario("Roberta", 2000D, "Marketing"));
        funcionarios.add(new Funcionario("Márcia", 3000D, "Contabilidade"));
        funcionarios.add(new Funcionario("Vera", 4000D, "Contabilidade"));
        funcionarios.add(new Funcionario("João", 5000D, "Recursos Humanos"));

        Map<String, Double> mapSalariosPorDepartamento = funcionarios.stream()
                .collect(Collectors.groupingBy(Funcionario::getDepartamento,
                        Collectors.summingDouble(Funcionario::getSalario)));

        for (String departamento : mapSalariosPorDepartamento.keySet()) {
            System.out.println(String.format("Total de salários em %s: %.2f", departamento, mapSalariosPorDepartamento.get(departamento)));
        }



    }
}
