import java.util.stream.IntStream;

public class Exercicio7 {

    public static void main(String[] args) {

        IntStream intStream = IntStream.of(1, 2, 2, 4, 4, 4, 7, 8, 9, 10);
//        intStream.distinct().filter(i -> i % 2 == 0).map(i -> i * i).forEach(System.out::println);
        intStream
                .distinct()
                .filter(i -> {
                    System.out.println("Filtrando " + i);
                    return i % 2 == 0;
                })
                .map(i -> {
                    System.out.println("Mapeando " + 1);
                    return i * i;
                })
                .forEach(System.out::println);
    }
}
