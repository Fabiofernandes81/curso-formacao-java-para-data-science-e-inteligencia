import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class Exercicio16 {

    public static void main(String[] args) {

        Random random = new Random(42);
        int[] numeros = random.ints(20000000, 1, 5000).toArray();
        IntStream intStream = Arrays.stream(numeros);

        long ti = System.currentTimeMillis();
        long totalElementos = intStream.parallel().filter(n -> n <= 1000).count();
        long tf = System.currentTimeMillis();
        System.out.println("Tempo total " + (tf - ti));
        System.out.println("Total de Elementos " + totalElementos);

    }
}
