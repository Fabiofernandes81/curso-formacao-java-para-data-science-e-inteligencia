import models.Jogador;
import models.Time;

import java.util.ArrayList;
import java.util.List;

public class Exercicio15 {

    public static void main(String[] args) {

        List<Time> times = new ArrayList<>();

        Time gremio = new Time("Grêmio");
        gremio.adicionarJogador(new Jogador("Renato Gaucho"));
        gremio.adicionarJogador(new Jogador("Paulo Nunes"));

        Time internacional = new Time("Internacional");
        internacional.adicionarJogador(new Jogador("Falcão"));
        internacional.adicionarJogador(new Jogador("Carpegiani"));

        times.add(gremio);
        times.add(internacional);

        times.stream().flatMap(t -> t.getJogadores().stream())
                .forEach(j -> System.out.println(j.getNome()));

    }
}
