public class Exercicio1 {

    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Código Executado dentro da Thread.");
            }
        }).start();

        new Thread(() ->
                System.out.println("Código Executado dentro da Thread.")
        ).start();
    }
}
