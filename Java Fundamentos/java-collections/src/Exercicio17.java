public class Exercicio17 {

    public static void main(String[] args) {

        Boneca boneca = new Boneca();
        boneca.setTitulo("Boneca elsa");
        boneca.setCorRoupa("Azul");

        Cofre<Brinquedo> bonecaCofre = new Cofre<>("123");
        bonecaCofre.abrir("123");
        bonecaCofre.guardar(boneca);
        bonecaCofre.fechar();

        bonecaCofre.abrir("123");
        Boneca bonecaGuardada = (Boneca) bonecaCofre.obter();
        bonecaCofre.fechar();

        System.out.println(bonecaGuardada.getTitulo() + " com roupa " + bonecaGuardada.getCorRoupa());
    }
}
