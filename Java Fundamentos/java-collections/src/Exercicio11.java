import java.util.Stack;

public class Exercicio11 {

    public static void main(String[] args) {

        Stack<Aluno> pilha = new Stack<>();
        pilha.push(new Aluno("Vera", 5));
        pilha.push(new Aluno("Clara", 8.5f));
        pilha.push(new Aluno("Melissa", 8));

        Aluno aluno = pilha.peek();
        System.out.println(aluno.getNome());

        aluno = pilha.pop();
        System.out.println(aluno.getNome());

    }
}
