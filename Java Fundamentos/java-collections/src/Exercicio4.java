public class Exercicio4 {

    public static void main(String[] args) {

        System.out.println("Heap size: " + Runtime.getRuntime().totalMemory());
        System.out.println("Heap max size" + Runtime.getRuntime().maxMemory());

        String resultadoStr = "";

        for (int i = 0; i <= 100000; i++) {
            resultadoStr = resultadoStr + " " + i;
            System.out.println("Free Memory " + Runtime.getRuntime().freeMemory());
        }

        System.gc();

        System.out.println("Fim!");


    }
}
