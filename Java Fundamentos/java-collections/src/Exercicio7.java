import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class Exercicio7 {

    public static void main(String[] args) {

        HashSet<String> cores = new HashSet<>();
        cores.add("vermelho");
        cores.add("verde");

        System.out.println("Inseriu amarelo? " + cores.add("amarelo"));
        System.out.println("Inseriu amarelo? " + cores.add("amarelo"));

        System.out.println(cores + " tem " + cores.size() + " elementos");

        for (String cor : cores) {
            System.out.println(cor);
        }

        System.out.println("---");

        TreeSet<String> cores2 = new TreeSet<>();
        cores2.add("vermelho");
        cores2.add("verde");
        cores2.add("amarelo");
        cores2.add("preto");
        cores2.add("laranja");

        for (String cor : cores2) {
            System.out.println(cor);
        }

        SortedSet<String> coresMenorQuePreto = cores2.headSet("preto");
        System.out.println(coresMenorQuePreto);

        coresMenorQuePreto = cores2.headSet("preto", true);
        System.out.println(coresMenorQuePreto);

        SortedSet<String> coresMaiorQuePreto = cores2.tailSet("preto");
        System.out.println(coresMaiorQuePreto);

        coresMaiorQuePreto = cores2.tailSet("preto", true);
        System.out.println(coresMaiorQuePreto);

        SortedSet<String> coresDoMeio = cores2.subSet(cores2.first(),false, cores2.last(),false);
        System.out.println(coresDoMeio);



    }
}
