import java.util.*;

public class Exercicio9 {

    public static void main(String[] args) {

        List<Instrutor> instrutorList = new ArrayList<>();
        instrutorList.add(new Instrutor(3, "Marcia", 1500));
        instrutorList.add(new Instrutor(1, "Aline", 1250));
        instrutorList.add(new Instrutor(2, "Vania", 3500));

        Collections.sort(instrutorList);

        for (Instrutor instrutor: instrutorList) {
            System.out.println(instrutor.getNome() + " => " + instrutor.getSalario());
        }

        System.out.println("---");

        Collections.sort(instrutorList, Comparator.comparing(Pessoa::getNome));

        for (Instrutor instrutor: instrutorList) {
            System.out.println(instrutor.getNome() + " => " + instrutor.getSalario());
        }
    }
}
