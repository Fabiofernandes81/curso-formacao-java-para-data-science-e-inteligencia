import java.util.ArrayList;
import java.util.List;

public class Exercicio16 {

    public static void main(String[] args) {

        Livro livro = new Livro("Senhor dos Anéis", 1000);

        CofreRuim cofreRuim = new CofreRuim();
        cofreRuim.guardar(livro);

        Livro livroDoCofre = (Livro) cofreRuim.obter();
        System.out.println(livro.getTitulo());

        List<Livro> livroList =  new ArrayList<>();
        livroList.add(livro);
        Livro livro1 = livroList.get(0);

        List<Boneca> bonecaList =  new ArrayList<>();
        bonecaList.add(new Boneca());
        Boneca boneca = bonecaList.get(0);
    }
}
