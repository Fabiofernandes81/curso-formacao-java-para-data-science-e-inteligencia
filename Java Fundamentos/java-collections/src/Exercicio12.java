import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Exercicio12 {

    public static void main(String[] args) {

        List<Integer> nums = new ArrayList<>();
        Random random = new Random(42);
        for (int i = 0; i <=10; i++){
            nums.add(random.nextInt());
        }

        System.out.println(nums);

        Collections.sort(nums);
        System.out.println(nums);

        Collections.shuffle(nums);
        System.out.println(nums);

        Collections.reverse(nums);
        System.out.println(nums);

        System.out.println(Collections.min(nums));
        System.out.println(Collections.max(nums));

        Collections.fill(nums,10);
        System.out.println(nums);

        System.out.println("---");

        List<Integer> nums2 = new ArrayList<>();
        for (int i = 1; i <=20; i++){
            nums2.add(i);
        }

        Collections.copy(nums2, nums);
        System.out.println(nums2);

        int quantidadeNumeros10 = Collections.frequency(nums2,10);
        System.out.println(quantidadeNumeros10);

        System.out.println(Collections.disjoint(nums, nums2));
    }
}
