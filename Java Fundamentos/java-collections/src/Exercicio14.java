import java.util.*;

public class Exercicio14 {

    public static void main(String[] args) {

        Map<Instrutor, Set<Aluno>> map = new HashMap<>();
        map.put(
                new Instrutor(2, "Daniel", 2000),
                new HashSet<>(
                        Arrays.asList(
                                new Aluno("Clara", 8),
                                new Aluno("Gislaine", 9)
                        )
                )
        );

        for (Instrutor instrutor : map.keySet()){
            for (Aluno aluno : map.get(instrutor)){
                System.out.println(aluno.getNome() + " é aluno de " + instrutor.getNome());
            }
        }
    }
}
