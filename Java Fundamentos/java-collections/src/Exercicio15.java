import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Exercicio15 {

    public static void main(String[] args) {

        List<String> cores = new ArrayList<>(Arrays.asList("Vermelho", "Amarelo", "Azul", "Verde"));

//        for (String cor: cores) {
//            if(cor.startsWith("A")){
//                cores.remove(cor);
//            }
//        }

        Iterator<String> iterator = cores.iterator();
        while (iterator.hasNext()){
            String cor = iterator.next();
            if(cor.startsWith("A")){
                iterator.remove();
            }
        }

        System.out.println(cores);

    }
}
