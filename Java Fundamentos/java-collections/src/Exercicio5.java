import java.util.ArrayList;

public class Exercicio5 {

    public static void main(String[] args) {

        ArrayList<String> alunos = new ArrayList<>();
        System.out.println("Tamanho: " + alunos.size());
        System.out.println("Lista está vazia?: " + alunos.isEmpty());

        alunos.add("João");
        alunos.add("José");

        System.out.println("Tamanho: " + alunos.size());
        System.out.println("Lista está vazia?: " + alunos.isEmpty());

        String nome = "Maria";
        alunos.add(nome);

        for (int i = 0; i < alunos.size(); i++) {
            System.out.println(alunos.get(i));
        }

        System.out.println("---");

        for (String aluno : alunos) {
            System.out.println(aluno);
        }

        System.out.println("---");

        System.out.println("Roberto está na lista? " + alunos.contains("Roberto"));
        System.out.println("Roberto está na lista? " + alunos.indexOf("Roberto"));
        System.out.println("Roberto está na lista? " + alunos.contains("João"));
        System.out.println("Roberto está na lista? " + alunos.indexOf("João"));
        alunos.add("João");
        System.out.println("Roberto está na lista? " + alunos.indexOf("João"));
        System.out.println("Roberto está na lista? " + alunos.lastIndexOf("João"));

        alunos.remove(0);
        alunos.remove(alunos.lastIndexOf("João"));
        alunos.remove("Maria");

        ArrayList<String> outrosAlunos = new ArrayList<>();
        outrosAlunos.add("Daniel");
        outrosAlunos.add("Higor");
        outrosAlunos.add("Suemar");

        alunos.addAll(outrosAlunos);

        for (String aluno : alunos) {
            System.out.println(aluno);
        }

        System.out.println("---");

        alunos.removeAll(outrosAlunos);

        alunos.set(0, "João da Silva");
        System.out.println(alunos.get(0));

        String[] alunosArray = new String[alunos.size()];
        alunosArray = alunos.toArray(alunosArray);

        for (String aluno: alunosArray) {
            System.out.println(aluno);
        }


    }
}
