public class Exercicio1 {

    public static void main(String[] args) {

        String nome = "João";
        nome = nome + " " + "Silva";
        System.out.println(nome);

        StringBuilder sb1 = new StringBuilder();

        StringBuffer sb2 = new StringBuffer(20);

        StringBuilder sb3 = new StringBuilder("DSA");

        System.out.println("Tamanho: " + sb3.length());
        System.out.println("Capacidade: " + sb3.capacity());

        sb3.ensureCapacity(100);
        System.out.println("Nova Capacidade: " + sb3.capacity());

        String formacao1 = "Formação Cientista de Dados";
        float precoFormacao1 = 2000;

        sb3.append(" possui formmação ").append(formacao1).append(" por ").append(precoFormacao1);
        System.out.println(sb3.toString());

        long ti = System.currentTimeMillis();

        String resultadoStr = "";
        for (int i = 0; i <= 50000; i++){
            resultadoStr = resultadoStr + " " + i;
        }

        long tf = System.currentTimeMillis();
        System.out.println("Tempo de Concentração de Strings: " + (tf - ti));

        ti = System.currentTimeMillis();

        StringBuilder resultadoSb = new StringBuilder(200000);
        for (int i = 0; i <= 50000; i++){
            resultadoSb.append(" ").append(i);
        }

        tf = System.currentTimeMillis();
        System.out.println("Tempo de Concentração de StringBuilder: " + (tf - ti));

    }
}
