import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Exercicio13 {

    public static void main(String[] args) {

        Map<Long, Aluno> mapPessoas = new HashMap<>();
        mapPessoas.put(1111111111L, new Aluno("Marcio", 5));
        mapPessoas.put(2222222222L, new Aluno("Maria", 6));
        mapPessoas.put(3333333333L, new Aluno("Roberto", 5.5f));

        mapPessoas.put(2222222222L, new Aluno("Maria Silva", 7));
        System.out.println(mapPessoas.get(2222222222L));

        System.out.println("Existe a chave 333?" + mapPessoas.containsKey(333));

        System.out.println(mapPessoas.remove(1));
        System.out.println(mapPessoas.remove(3333333333L));

        System.out.println("---");

        for (Long chave: mapPessoas.keySet()) {
            System.out.println(mapPessoas.get(chave));
        }

        System.out.println("---");

        Collection<Aluno> alunoCollection = mapPessoas.values();
        System.out.println(alunoCollection);

        mapPessoas.clear();
        System.out.println(mapPessoas.size());

    }
}
