import java.util.ArrayList;

public class Exercicio6 {

    public static void main(String[] args) {

        ArrayList<Pessoa> pessoaList = new ArrayList<>();
        Aluno joao = new Aluno("João", 10);
        pessoaList.add(joao);

        pessoaList.add(new Aluno("José", 6));
        pessoaList.add(new Instrutor(1, "Sumear", 1000));

        Pessoa pessoa = pessoaList.get(0);
        System.out.println(pessoa.getNome());

        Instrutor instrutor = (Instrutor) pessoaList.get(2);
        System.out.println(instrutor.getNome() + " recebe " + instrutor.getSalario());

        for (Pessoa pes : pessoaList) {
            if (pes instanceof Aluno) {
                System.out.println("O aluno " + pes.getNome() + " tem nota " + ((Aluno) pes).getNota());
            } else if (pes instanceof Instrutor) {
                System.out.println("O instrutor " + pes.getNome() + " recebe " + ((Instrutor) pes).getSalario());
            }
        }

    }
}
