public class Bola extends Brinquedo {

    private float diametro;

    public float getDiametro() {
        return diametro;
    }

    public void setDiametro(float diametro) {
        this.diametro = diametro;
    }
}
