import java.util.LinkedList;
import java.util.Queue;

public class Exercicio10 {

    public static void main(String[] args) {

        Queue fila = new LinkedList();
        fila.offer(new Instrutor(3, "Marcia", 1500));
        fila.offer(new Instrutor(1, "Aline", 1250));
        fila.offer(new Instrutor(2, "Vania", 3500));

        Instrutor instrutor = (Instrutor) fila.peek();
        System.out.println(instrutor.getNome());
        System.out.println(fila.size());

        instrutor = (Instrutor) fila.poll();
        System.out.println(instrutor.getNome());
        System.out.println(fila.size());

        instrutor = (Instrutor) fila.remove();
        System.out.println(instrutor.getNome());
        System.out.println(fila.size());

        instrutor = (Instrutor) fila.poll();
        instrutor = (Instrutor) fila.poll();
        instrutor = (Instrutor) fila.poll();

        Queue<Aluno> alunos = new LinkedList<>();
        alunos.offer(new Aluno("Fabio", 8));


    }
}
