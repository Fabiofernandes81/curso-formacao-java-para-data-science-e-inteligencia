public class Exercicio2 {

    public static void main(String[] args) {

        int idade =10;
        //idade = null;

        Integer novaIdade = 20;

        System.out.println("Nova idade é " + novaIdade);

        int idadePrimitivo = novaIdade;

        System.out.println("Idade Primitivo: " + idadePrimitivo);

        novaIdade = null;

        System.out.println("Nova idade: " + novaIdade);

        idadePrimitivo = novaIdade;

    }
}
