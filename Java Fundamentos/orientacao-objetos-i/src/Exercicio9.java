import dsa.Conta;

public class Exercicio9 {

    public static void main(String[] args) {

        Conta conta = new Conta();
        conta.deposita(500);
        System.out.println(conta.getSaldo());

        conta.deposita(300,0);
        System.out.println(conta.getSaldo());

        conta.deposita(300, 10F);
        System.out.println(conta.getSaldo());
    }
}
