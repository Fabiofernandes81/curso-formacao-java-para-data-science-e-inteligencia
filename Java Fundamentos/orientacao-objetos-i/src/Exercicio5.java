import dsa.Conta;

public class Exercicio5 {

    public static void main(String[] args) {

        Conta conta = new Conta();
        conta.titular = "Fabio";
        conta.deposita(1000);
        conta.saca(200);
        conta.saca(900);

        System.out.println(conta.getSaldo());

    }
}
