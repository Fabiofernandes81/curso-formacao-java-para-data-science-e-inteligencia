package dsa;

public class Conta {

    public String titular;

    private float saldo;

    public Conta(){
        this.saldo = 5;
    }

    public Conta(String titular){
        this();
        this.titular = titular;
    }

    public float getSaldo() {
        return saldo;
    }

    public void saca(float valor) {
        if (valor <= 0) {
            throw new IllegalArgumentException("Valor inválido para o saque " + valor);
        } else if (saldo - valor < 0) {
            throw new IllegalArgumentException("Saldo insuficiente " + saldo);
        }
        saldo -= valor;
    }

    public void deposita(float valor) {
        if (valor < 0) {
            throw new IllegalArgumentException("Valor inválido para o depósito " + saldo);
        } else {
            saldo += valor;
        }
    }

    public void deposita(float valor, int emQuantosDias) {
        if(emQuantosDias ==0){
            deposita(valor);
        }else{
            System.out.println("Depósito Agendado.");
        }
    }

    public void deposita(float valor, float bonus) {
        deposita(valor + bonus);
    }
}
