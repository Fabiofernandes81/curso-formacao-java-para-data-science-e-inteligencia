public class Exercicio12 {

    private static int anosParaCompletar100Anos(int idade) {

        if (idade < 0) {
            throw new IdadeMenorQue0Exception("Idade não pode ser negativa!");
        }

        if (idade > 100) {
            throw new IdadeMaiorQue100Exception("Idade não pode ser maior que 100!");
        }

        return 100 - idade;
    }

    public static void main(String[] args) {
        try {
            int anosRestantes = anosParaCompletar100Anos(37);
            System.out.println(anosRestantes);
        }catch (IllegalArgumentException e) {
            System.out.println("Valor de idade inválido!" + e.getMessage());
        }
    }

}

class IdadeMaiorQue100Exception extends IllegalArgumentException {
    IdadeMaiorQue100Exception(String message){
        super(message);
    }
}

class IdadeMenorQue0Exception extends IllegalArgumentException {
    IdadeMenorQue0Exception(String message){
        super(message);
    }
}
