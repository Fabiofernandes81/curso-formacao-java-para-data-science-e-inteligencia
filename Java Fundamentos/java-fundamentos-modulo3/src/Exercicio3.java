import java.util.Arrays;
import java.util.Random;

public class Exercicio3 {

    public static void main(String[] args){

        Random random = new Random();

        int[] array1 = new int[100000];

        int[] array2 = new int[100000];

        for (int i = 0; i < array1.length; i++){
            array1[i] = random.nextInt(100);
            array2[i] = array1[i];
        }

        long inicioTarefa = System.currentTimeMillis();
        Arrays.sort(array1);
        long fimTarefa = System.currentTimeMillis();
        System.out.println("Ordenação do Array 1: " + (fimTarefa - inicioTarefa));

        inicioTarefa = System.currentTimeMillis();

    }
}
