import java.lang.reflect.Array;
import java.util.Arrays;

public class Exercicio2 {

    public static void main(String[] args){

        String[] nomes = {"Daniel", "Higor", "Suemar"};

        int[] idades = {10, 20, 30};

        Arrays.fill(idades, 10);

        idades[0] = 30;
        idades[1] = 10;
        idades[2] = 20;

        int numeroAProcurar = 20;
        boolean achou = false;

        for(int idade : idades){
            if(idade == numeroAProcurar) {
                achou = true;
                break;
            }
        }

        System.out.println("Número " + numeroAProcurar + " está na lista? " + achou);

        Arrays.sort(idades);

        int pontoInsercao = Arrays.binarySearch(idades, 20);
        System.out.println(pontoInsercao);

        for (int idade : idades){
            System.out.println(idade);
        }



    }
}
