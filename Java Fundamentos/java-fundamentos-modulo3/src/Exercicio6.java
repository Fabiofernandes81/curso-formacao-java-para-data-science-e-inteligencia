public class Exercicio6 {

    public static void main(String[] args){

        int[] salarariosDiretores = { 10000, 20000, 30000};
        int[] salarariosExecutivos = { 40000, 50000, 60000};

        int[] salariosTodos = somaArrays(salarariosDiretores, salarariosExecutivos);

        for (int salario : salariosTodos){
            System.out.println(salario);
        }

    }

    private static int[] somaArrays(int[] a, int[] b){

        int[] resultado = new int[a.length + b.length];

        int i = 0;

        for (int num : a){
            resultado[i] = num;
            i++;
        }

        for (int num : b){
            resultado[i] = num;
            i++;
        }

        return resultado;
    }
}
