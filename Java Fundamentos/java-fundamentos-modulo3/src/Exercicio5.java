public class Exercicio5 {

    public static void main(String[] args){

        int total = 0;

        for (String arg : args){
            total += Integer.parseInt(arg);
        }

        System.out.println("Quantidde de parametros: " + args.length);

        System.out.println("Total: " + total);

    }
}
