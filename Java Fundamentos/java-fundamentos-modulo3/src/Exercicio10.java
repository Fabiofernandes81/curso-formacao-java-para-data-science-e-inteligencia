import java.io.*;

public class Exercicio10 {

    public static void imprimeArquivo(String nomeArquivo) throws IOException {


        FileInputStream fileInputStream = new FileInputStream(nomeArquivo);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        String line = null;

        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        bufferedReader.close();

    }

    public static void main(String[] args) {
        try {
            imprimeArquivo("src/Exercicio0.java");
        } catch (IOException e) {
            System.out.println("Ocorreu um erro!");
        }
    }
}
