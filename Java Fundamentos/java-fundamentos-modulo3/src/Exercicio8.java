import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercicio8 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Vamos fazer a divisão entre 2 números...");

        try {
            System.out.println("Qual o primeiro número?");
            int num1 = scanner.nextInt();

            System.out.println("Qual o segundo número?");
            int num2 = scanner.nextInt();


            int resultado = num1 / num2;
            System.out.println(String.format("Resultado: %s", resultado));
        } catch (ArithmeticException e) {
            System.out.println(String.format("Ocorreu um erro! %s", e.getMessage()));
        } catch (InputMismatchException e){
            System.out.println(String.format("Número informado é inválido! %s", e.getMessage()));
        }
        finally {
            System.out.println("Fim");
        }


    }
}
