public class Exercicio7 {

    private static int soma2Numeros(int num1, int num2) {
        return num1 + num2;
    }

    private static int soma3Numeros(int num1, int num2, int num3) {
        return num1 + num2 + num3;
    }

    private static int somaNumero(int... numeros) {
        int total = 0;
        for (int numero : numeros) {
            total += numero;
        }
        return total;
    }

    public static void main(String[] args) {

        System.out.println(soma2Numeros(10, 20));
        System.out.println(soma3Numeros(10, 20, 30));
        System.out.println(somaNumero(10, 20, 30, 40, 50, 60));
        System.out.println(somaNumero(10, 20, 30, 40, 50, 60, 70, 80, 90));

    }
}
