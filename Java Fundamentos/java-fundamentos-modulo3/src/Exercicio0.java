public class Exercicio0 {

    public static void main(String[] args){

        String flag =  String.valueOf(true);

        String idadeEmString = String.valueOf(20);

        String salarioEmString = String.valueOf(1234.56);

        char[] arrayChar = {'D', 'S', 'A'};

        String nome = String.valueOf(arrayChar);

        int idade = Integer.parseInt("10");

        float salario = Float.parseFloat("123.45");

        double salarioMuitoGrande = Double.parseDouble("123456.78");

        int salarioTruncado = (int) 12345.67F;

        short meuShort = 10;

        int meuInteiro = meuShort;

        meuInteiro = 200000000;

        meuShort = (short) meuInteiro;

        System.out.println(meuShort);


    }
}
