public class Exercicio1 {

    public static void main(String[] args) {

        double salario1 = 123.0, salario2 = 456, salario3 = 789.0;

        double salarios[];

        salarios = new double[5];

        salarios[3] = 1000;
        salarios[4] = 2000;

        for (int i = 0; i < salarios.length; i++) {
            salarios[i] = 1000*i;
        }

        double totalSalarios = 0;
        for (double salario: salarios) {
            totalSalarios += salario;
        }

        double mediaSalarios = totalSalarios /salarios.length;

        System.out.println(String.format("Média Salarial: %f", mediaSalarios));

    }
}
