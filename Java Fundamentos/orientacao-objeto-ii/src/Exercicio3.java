import dsa.Executivo;
import dsa.Funcionario;

public class Exercicio3 {

    public static void main(String[] args) {

        Executivo executivo = new Executivo("Fabio", "Fernandes","Desenvolvedor", 1000F);

        System.out.println(executivo.getNomeCompleto() + " terá um bônus de " + executivo.getBonus());

    }
}
