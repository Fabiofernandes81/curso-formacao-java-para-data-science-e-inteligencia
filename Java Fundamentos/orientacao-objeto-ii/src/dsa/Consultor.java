package dsa;

import dsa.interfaces.Palestrante;

public class Consultor extends Pessoa implements Palestrante {

    private String especialidade;

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    @Override
    public float getBonusComPalestras() {
        return 1000;
    }

    @Override
    public float getGastosComPalestras() {
        return 0;
    }
}
