package dsa.interfaces;

public interface Palestrante {

    float getBonusComPalestras();

    float getGastosComPalestras();

}
