package dsa;

public class Calculadora {

    private static int totalCalculadoras = 0;

    public static int soma(int num1, int num2) {
        return num1 + num2;
    }

    public Calculadora() {
        totalCalculadoras++;
    }

    public static int getTotalCalculadoras() {
        return totalCalculadoras;
    }
}
