package dsa;

import dsa.interfaces.Palestrante;

public class Executivo extends Funcionario implements Palestrante {

    private static final int BONUS_PERCENTUAL = 10;

    public Executivo(String nome, String sobreNome, String cargo, float salario){
        super(nome, sobreNome, cargo, salario, new Conta());
    }

    @Override
    public float getBonus() {
        return this.salario /100 * BONUS_PERCENTUAL;
    }

    @Override
    public float getBonusComPalestras() {
        return 3000;
    }

    @Override
    public float getGastosComPalestras() {
        return 0;
    }
}
