package dsa;

public class Funcionario extends Pessoa {

    private static final int TAXA_ADMINISTRACAO_BONUS = 50;

    private static final int BONUS_PERCENTUAL = 2;

    private String cargo;

    protected float salario;

    private Conta conta;

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }



    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Funcionario(){

    }

    public Funcionario(String nome, String sobreNome, String cargo, float salario, Conta conta) {
        super(nome, sobreNome);
        this.cargo = cargo;
        this.salario = salario;
    }

    public float getBonus(){
        return (this.salario - TAXA_ADMINISTRACAO_BONUS) / 100 * BONUS_PERCENTUAL;
    }
}
