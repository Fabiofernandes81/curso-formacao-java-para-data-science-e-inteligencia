import dsa.Aluno;

public class Exercicio11 {

    public static void main(String[] args) {

        Aluno aluno1 = new Aluno();
        aluno1.setNumeroMatricula(2);
        aluno1.setNome("Fabio");

        Aluno aluno2 = new Aluno();
        aluno2.setNumeroMatricula(1);
        aluno2.setNome("Fabio");

        if (aluno1.equals(aluno2)) {
            System.out.println("É o mesmo aluno");
        } else {
            System.out.println("Não é o mesmo aluno");
        }
    }
}
