import dsa.Aluno;

public class Exercicio13 {

    public static void main(String[] args) {

        Aluno aluno = new Aluno();
        aluno.setNome("João");
        aluno.setSobreNome("Silva");
        aluno.setNumeroMatricula(1);

        System.out.println(String.format("Nome completo do aluno: %s", aluno.getNomeCompleto()));

    }
}
