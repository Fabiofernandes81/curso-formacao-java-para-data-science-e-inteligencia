import dsa.*;

import java.util.Date;

public class Exercicio9 {

    public static void main(String[] args) {

        Consultor consultor = new Consultor();
        consultor.setNome("Thiago");

        Palestra2 palestra = new Palestra2("Machine Learning", new Date(), "São Paulo", consultor);
        System.out.println("O consultor " + consultor.getNome() + " receberá bônus de " + consultor.getBonusComPalestras());

        Executivo executivo = new Executivo("Marcos", "Silva", "CEO", 5000F);
        System.out.println("O executivo " + executivo.getNome() + " receberá bônus de " + executivo.getBonusComPalestras());

        Instrutor instrutor = new Instrutor();
        instrutor.setNome("Roberta");
        instrutor.setCurso("Visão Computacional");
        System.out.println("O instrutor(a) " + instrutor.getNome() + " receberá bônus de " + instrutor.getBonusComPalestras());

        Aluno aluno = new Aluno();
        aluno.setNome("Silvio");

        Palestra2 palestraDS = new Palestra2("Data Science", new Date(), "Cuiabá", aluno);
        System.out.println("O aluno(a) " + aluno.getNome() + " receberá bônus de " + aluno.getBonusComPalestras());

    }
}
