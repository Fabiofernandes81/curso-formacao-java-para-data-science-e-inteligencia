import dsa.Executivo;
import dsa.Funcionario;
import dsa.Palestra;

import java.util.Date;

public class Exercicio7 {

    public static void main(String[] args) {

        Executivo daniel = new Executivo("Daniel", "Mendes", "CEO", 2000F);

        Funcionario suemar = new Executivo("Suemar", "Everton", "Desenvolvedor", 1000F);

        Palestra palestra = new Palestra("Java", new Date(), "São Paulo", suemar);

        System.out.println(palestra.getPalestrante().getNome() + " dará palestra de " + palestra.getTitulo());

        System.out.println(palestra.getPalestrante().getNome() + " receberá " + palestra.getPalestrante().getBonus());

    }
}
