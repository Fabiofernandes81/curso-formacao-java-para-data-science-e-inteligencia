import dsa.Funcionario;

public class Exercicio1 {

    public static void main(String[] args) {

        Funcionario funcionario = new Funcionario();
        funcionario.setNome("Fabio");
        funcionario.setSobreNome("Fernandes");
        funcionario.setCargo("Desenvolvedor");

        System.out.println(funcionario.getNomeCompleto() + " é um " + funcionario.getCargo());
    }
}
