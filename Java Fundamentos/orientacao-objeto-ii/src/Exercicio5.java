import dsa.Conta;
import dsa.Funcionario;

public class Exercicio5 {

    public static void main(String[] args) {

        Funcionario funcionario = new Funcionario("Fabio", "Fernandes", "Desenvolvedor", 1000F, new Conta());

        System.out.println(funcionario.getConta().getSaldo());
    }
}
