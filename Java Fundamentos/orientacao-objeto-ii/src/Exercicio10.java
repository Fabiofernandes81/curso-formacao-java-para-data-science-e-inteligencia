public class Exercicio10 {

    public static void main(String[] args) {

        String curso1 = new String("Java Fundamentos");

        String curso2 = new String("Java Fundamentos");

        if (curso1.equals(curso2)) {
            System.out.println("Cursos são iguais");
        } else {
            System.out.println("Cursos são diferentes");
        }
    }
}
