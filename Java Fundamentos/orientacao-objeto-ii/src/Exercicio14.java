import dsa2.Animal;
import dsa2.Cachorro;
import dsa2.Gato;
import dsa2.Pato;

public class Exercicio14 {

    public static void main(String[] args) {

        Pato pato = new Pato();
        System.out.println(pato.som());

        Gato gato = new Gato();
        System.out.println(gato.som());

        Animal animal = new Cachorro();
        System.out.println(animal.som());

    }
}
