import dsa.Conta;
import dsa.Funcionario;

public class Exercicio2 {

    public static void main(String[] args) {

        Funcionario funcionario = new Funcionario("Fabio", "Fernandes","Desenvolvedor", 1000F, new Conta());

        System.out.println(funcionario.getNomeCompleto() + " é um " + funcionario.getCargo());

    }
}
