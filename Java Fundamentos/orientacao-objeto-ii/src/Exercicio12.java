import dsa.Consultor;
import dsa.Palestra2;
import dsa.interfaces.Palestrante;

import java.util.Date;

public class Exercicio12 {

    public static void main(String[] args) {

        Palestra2 palestra = new Palestra2("Machine Learning", new Date(), "Campo Grande", new Palestrante() {
            @Override
            public float getBonusComPalestras() {
                return 200;
            }

            @Override
            public float getGastosComPalestras() {
                return 0;
            }
        });

        Palestrante palestrante = palestra.getPalestrante();
        System.out.println(palestrante.getBonusComPalestras());
    }
}
