import model.Curso;
import model.Instrutor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Exercicio7 {

    public static void main(String[] args) {

        Instrutor instrutor = new Instrutor(1, "Suemar", 1000);
        Curso curso = new Curso(1,"Java", instrutor, 400);

        try{
            FileOutputStream fileOutputStream = new FileOutputStream("curso.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(curso);
            objectOutputStream.close();
            fileOutputStream.close();
            System.out.println("Objeto Serializado");
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
