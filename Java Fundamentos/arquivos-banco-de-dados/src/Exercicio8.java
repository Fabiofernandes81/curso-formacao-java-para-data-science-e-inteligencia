import model.Curso;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Exercicio8 {

    public static void main(String[] args) {
        Curso curso = null;

        try{
            FileInputStream fileInputStream = new FileInputStream("curso.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            curso = (Curso) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

        System.out.println("Objeto deserealizado");
        System.out.println("Curso : " + curso.getNome());
        System.out.println("Instrutor: " + curso.getInstrutor().getNome());
    }
}
