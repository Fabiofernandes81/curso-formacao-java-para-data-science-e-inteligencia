import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Exercicio9 {

    public static void main(String[] args) {

        URL url = null;
        BufferedReader bufferedReader = null;

        try {
            url = new URL("https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
            bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));

            String inputLine = null;

            while ((inputLine = bufferedReader.readLine()) != null) {
                System.out.println(inputLine);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
