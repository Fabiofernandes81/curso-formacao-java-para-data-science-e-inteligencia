import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercicio2 {

    public static void main(String[] args) {
        try{
            FileReader fileReader = new FileReader("src/arquivo_de_teste.txt");
            int data = fileReader.read();
            while (data!=-1){
                System.out.println(data + " " + (char)data);
                data = fileReader.read();
            }
            fileReader.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }


    }
}
