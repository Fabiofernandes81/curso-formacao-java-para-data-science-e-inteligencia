import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Exercicio1 {

    public static void main(String[] args) {

        File dir = new File("src");
        System.out.println("src existe? " + dir.exists());
        System.out.println("src é um diretório? " + dir.isDirectory());
        System.out.println("src é um arquivo? " + dir.isFile());

        try{
            System.out.println("Path de src é " + dir.getCanonicalPath());
        }catch (IOException e){
            e.printStackTrace();
        }

        for (String arquivo : dir.list()){
            System.out.println(arquivo);
        }

        System.out.println("---");

        String pathName = "src" + File.separator + "Exercicio1.java";
        File fOrigem = new File(pathName);

        System.out.println("Arquivo de origem existe?" + fOrigem.exists());
        System.out.println("Tamanho?" + fOrigem.length());
        System.out.println("Posso ler?" + fOrigem.canRead());
        System.out.println("Posso escrever?" + fOrigem.canWrite());
        System.out.println("Posso executar?" + fOrigem.canExecute());
    }
}
