import java.io.File;
import java.lang.reflect.Field;

public class Exercicio6 {

    public static void main(String[] args) {
        File file = new File("src/tabuada.txt");
        System.out.println(file.exists());
        System.out.println("Excluiu?" + file.delete());

        try{
            file.createNewFile();
            System.out.println("Arquivo novo criado");
        }catch (Exception e){
            e.printStackTrace();
        }

        File dir = new File("src/exemplo1");
        System.out.println(dir.mkdir());

        File dir2 = new File("src/exemplo2/nivel1/nivel2/nivel3");
        System.out.println(dir2.mkdirs());

    }




}
