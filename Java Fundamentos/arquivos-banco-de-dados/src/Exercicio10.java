import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Exercicio10 {

    public static void main(String[] args) {

        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("Driver carregado");
        }catch (Exception e){
            System.out.println("Erro ao carregar o driver" + e.getMessage());
        }

        Connection con = null;

        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost/dsa?user=fabio&password=pass123&serverTimezone=UTC");
            System.out.println("Conexão estabelecida.");
        }catch (SQLException e){
            System.out.println("SQL Exception " + e.getMessage());
        }

    }
}
